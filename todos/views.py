from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    count = TodoList.objects.count()
    context = {
        "lists": todo_lists,
        "count": count,
    }
    return render(request, "todo_lists/list.html", context)


def todo_list_detail(request, id):
    tasks = get_object_or_404(TodoList, id=id)
    context = {"todo": tasks}
    return render(request, "todo_lists/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todo_list_detail", id=TodoList.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todo_lists/create.html", context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoListForm(instance=list)

    context = {"form": form}

    return render(request, "todo_lists/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todo_lists/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)

    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todo_lists/create_todo.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)

    else:
        form = TodoItemForm(instance=item)

    context = {"form": form}

    return render(request, "todo_lists/edit_todo_item.html", context)
